class MyBaseClass
{
 public:
    MyBaseClass();
    ~MyBaseClass();

    virtual void SomeMethod() = 0;
}