#include "MyBaseClass.hpp"


class MyClass : public MyBaseClass
{
 public:
    MyClass();
    ~MyBaseClass();

    virtual void SomeMethod();

}