# Infrasture for design document processing
## What is our goal?
We want to create an infrastructure for processing of design documentation in gitlab. It needs to facilitate a very natural and easy way of writing, editing and viewing the documents. User should not require any additional software, or tools apart of access to the repository. Documents should allow the user to include graphics both as images and as a PlantUML diagrams. 

Documentation should be processed automatically and published on the project webpage for easy access. The most vesatile way that follows modern standard is to containerize the processing and allow it to be run on gitlab runner. For that we will use Docker.

## Test of PlantUML diagrams

```puml
@startuml
Bob -> Alice : hello
Alice -> Bob : hi
@enduml
```

## TODO
- [ ] Prepare a docker image in a CI job
- [X] Add PlantUML diagrams into .md file in the repo. Check if it renders in web view
- [X] Add a simple CI job to build a very simple webpage and publish that to Gitlab Pages
- [ ] Define a list of things to do for the presentation
- [ ] Find a way to embedd a changelog in the pages
- [ ] Find a way to add git trigger to require a changelog tag
- [ ] Check if we can link from md file to a story
- [ ] JIRA number in change log?
- [ ] Find which tools for processing are available
- [X] Choose a tool
- [ ] Add to the docs the good sources for markdown info
- [X] Gather all good links
- [X] Possibly make the PlantUML accessible as a service in the gitlab ci
- [X] Check what is SuperFences and how does it compare to Fenced Code Blocks
- [ ] Add MagicLink extension config for GitLab etc.
- [ ] Investigate how to bridge doxygen into mkdocs
- [ ] Investigate pdf export and embedd on webpage using the mkdocs-pdf-export-plugin
 

## List of available MD processors that we could use
### Read the Docs  
It seems to be a SaaS tool, so it may be less desirable. There is a buissness version, but it still is not a great idea. Underneath there are different engines used, but one of them is MkDocs

### MkDocs
- Single YAML configuration (easy)
- Has some predefined themes (including Read The Docs theme)
- Can be integrated with PlantUML if a service is availabe on the network
- Can have a nice material theme - looks good
- Has many plugins, including one for processing code blocks and syntax coloring
- 

### Docusaurus
- Code coloring
- Diagrams using marmaid, maybe (!) could be forced to be used with plantuml

### VuePress
- Nice looking 
- Code coloring
- Can't seem to find plantuml integration
- Creates dynamic site and then renders each page to make a "static" page
- Seems to be a bit too "modern" - complicated in the idea 

### Jekyll
- Doesn't seem to support diagrams


## Steps to take
- [X] Create a docker for processing docs
- [X] Publish a non final docs to pages
- [ ] Add and "index"
- [ ] Publish a more final docs to pages
- [ ] Make a CI job to build a container for processing job
- [X] Add a configuration files for the processor in repo


## List of links to follow
- https://www.markdownguide.org/tools/
- https://squidfunk.github.io/mkdocs-material
- https://www.mkdocs.org/
- https://medium.com/@davide.gazze/how-to-use-plantuml-inside-mkdocs-3b6ac0a8c757
- https://docs.gitlab.com/ee/ci/services/
- https://plantuml.com
- https://hub.docker.com/r/plantuml/plantuml-server
- https://jmp75.github.io/work-blog/documentation/c++/python/mkdocs/2022/08/22/doxygen-doxybook-mkdocs.html
- https://github.com/zhaoterryy/mkdocs-pdf-export-plugin
- https://betterprogramming.pub/automate-changelog-generation-with-gitlab-dbd4e4c41d99